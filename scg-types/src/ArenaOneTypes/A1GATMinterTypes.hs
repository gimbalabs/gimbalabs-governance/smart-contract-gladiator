
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DerivingVia           #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE ImportQualifiedPost   #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NamedFieldPuns        #-}
{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE UndecidableInstances  #-}

module  ArenaOneTypes.A1GATMinterTypes
            (   Arena1'GATParams  (..)
            ,   Arena1'GATAction  (..)
            )
                where

import           GHC.Generics             (Generic)
import           Plutus.V1.Ledger.Crypto  (PubKeyHash)
import           Plutus.V1.Ledger.Scripts (ValidatorHash)
import qualified PlutusTx
import qualified Prelude                  as Haskell

data Arena1'GATParams = Arena1'GATParams
    {   gateKeeperVH            ::  ValidatorHash
    ,   a1'GATMinterDebuggerPKH ::  PubKeyHash
    }
    deriving
        (Haskell.Eq, Haskell.Ord, Haskell.Show, Generic)

PlutusTx.makeLift ''Arena1'GATParams


data Arena1'GATAction
    =   GateKeeperAdminMintA1'GAT
    |   BurnA1'GAT
    |   A1'GATMinterDebugger
    deriving (Haskell.Show)


PlutusTx.makeIsDataIndexed ''Arena1'GATAction   [ ( 'GateKeeperAdminMintA1'GAT,     1 )
                                                , ( 'BurnA1'GAT,                    2 )
                                                , ( 'A1'GATMinterDebugger,          3 )
                                                ]
