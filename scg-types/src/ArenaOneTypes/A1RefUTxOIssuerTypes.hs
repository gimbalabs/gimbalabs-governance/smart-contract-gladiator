
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DerivingVia           #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE ImportQualifiedPost   #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NamedFieldPuns        #-}
{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE UndecidableInstances  #-}

module  ArenaOneTypes.A1RefUTxOIssuerTypes
            (   Arena1'RefUTxOIssuerParams   (..)
            ,   Arena1'RefUTxOIssuerDatum    (..)
            ,   Arena1'RefUTxOIssuerAction   (..)
            )
                where

import           GHC.Generics             (Generic)
import           Plutus.V1.Ledger.Address (Address)
import           Plutus.V1.Ledger.Crypto  (PubKeyHash)
import           Plutus.V1.Ledger.Scripts (ScriptHash, ValidatorHash)
import           Plutus.V1.Ledger.Value   (CurrencySymbol)
import qualified PlutusTx
import           PlutusTx.Prelude         (Integer)
import qualified Prelude                  as Haskell

data Arena1'RefUTxOIssuerParams = Arena1'RefUTxOIssuerParams
    {   accessControlVH                 ::  ValidatorHash
    ,   senatePolicyID                  ::  CurrencySymbol
    ,   arena1'RefUTxOIssuerDebuggerPKH ::  PubKeyHash
    }
    deriving
        (Haskell.Eq, Haskell.Ord, Haskell.Show, Generic)

PlutusTx.makeLift ''Arena1'RefUTxOIssuerParams

data  Arena1'RefUTxOIssuerDatum
    =   Arena1'RefUTxOIssuerScriptActivatorDatum
            {   _polymorphicNumber :: Integer
            }
    |   Arena1'GateKeeperRefUTxODatum
            {   republicTreasuryAddress    :: Address
            ,   arena1'ProxySH             :: ScriptHash
            ,   currencySymbolOfArena1'GAT :: CurrencySymbol
            }
    |   Arena1'ProxyRefUTxODatum
            {   currencySymbolOfArena1'GAT    ::  CurrencySymbol
            }
    |   A1'GATMinterRefUTxODatum
            {   arena1'ProxyVH    ::  ValidatorHash
            }
        deriving
            (Haskell.Eq, Haskell.Ord, Haskell.Show, Generic)

PlutusTx.makeIsDataIndexed '' Arena1'RefUTxOIssuerDatum    [ ( 'Arena1'RefUTxOIssuerScriptActivatorDatum, 1 )
                                                            , ( 'Arena1'GateKeeperRefUTxODatum,             2 )
                                                            , ( 'Arena1'ProxyRefUTxODatum,                  3 )
                                                            , ( 'A1'GATMinterRefUTxODatum,                  4 )
                                                            ]

data  Arena1'RefUTxOIssuerAction
    =   IssueArena1'GateKeeperRefUTxO
    |   IssueArena1'ProxyRefUTxO
    |   IssueA1'GATMinterRefUTxO
    |   SendBackScriptActivatorUTxO
    |   Arena1'RefUTxOIssuerDebugger
    deriving (Haskell.Show)

PlutusTx.makeIsDataIndexed ''Arena1'RefUTxOIssuerAction [ ( 'IssueArena1'GateKeeperRefUTxO,    1 )
                                                            , ( 'IssueArena1'ProxyRefUTxO,      2 )
                                                            , ( 'IssueA1'GATMinterRefUTxO,      3 )
                                                            , ( 'SendBackScriptActivatorUTxO,   4 )
                                                            , ( 'Arena1'RefUTxOIssuerDebugger, 5 )
                                                            ]
