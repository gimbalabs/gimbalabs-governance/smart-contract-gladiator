
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DerivingVia           #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE ImportQualifiedPost   #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NamedFieldPuns        #-}
{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE UndecidableInstances  #-}

module  ArenaOneTypes.A1DATMinterTypes
            (   Arena1'DATParams  (..)
            ,   Arena1'DATAction  (..)
            )
                where

import           GHC.Generics             (Generic)
import           Plutus.V1.Ledger.Crypto  (PubKeyHash)
import           Plutus.V1.Ledger.Scripts (ValidatorHash)
import qualified PlutusTx
import qualified Prelude                  as Haskell

data Arena1'DATParams = Arena1'DATParams
    {   accessControlVH         ::  ValidatorHash
    ,   a1'DATMinterDebuggerPKH ::  PubKeyHash
    }
    deriving
        (Haskell.Eq, Haskell.Ord, Haskell.Show, Generic)

PlutusTx.makeLift ''Arena1'DATParams


data Arena1'DATAction
    =   GovernorMintA1'DAT
    |   GovernorBurnA1'DAT
    |   A1'DATMinterDebugger
    deriving (Haskell.Show)


PlutusTx.makeIsDataIndexed ''Arena1'DATAction   [ ( 'GovernorMintA1'DAT,    1 )
                                                , ( 'GovernorBurnA1'DAT,    2 )
                                                , ( 'A1'DATMinterDebugger,  3 )
                                                ]
