
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DerivingVia           #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE ImportQualifiedPost   #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NamedFieldPuns        #-}
{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE UndecidableInstances  #-}

module  ArenaOneTypes.A1ProxyTypes
            (   Arena1'ProxyParams (..)
            ,   Arena1'ProxyDatum  (..)
            ,   Arena1'ProxyAction (..)
            )
                where

import           GHC.Generics             (Generic)
import           Plutus.V1.Ledger.Crypto  (PubKeyHash)
import           Plutus.V1.Ledger.Scripts (ValidatorHash)
import           Plutus.V1.Ledger.Time    (POSIXTime)
import           Plutus.V1.Ledger.Value   (CurrencySymbol)
import qualified PlutusTx
import           PlutusTx.Prelude         (Bool (..), Eq, (&&), (==))
import qualified Prelude                  as Haskell

data Arena1'ProxyParams = Arena1'ProxyParams
    {   gateKeeperVH            ::  ValidatorHash
    ,   arena1'ProxyDebuggerPKH ::  PubKeyHash
    }
    deriving
        (Haskell.Eq, Haskell.Ord, Haskell.Show, Generic)

PlutusTx.makeLift ''Arena1'ProxyParams

data Arena1'ProxyDatum
    =   Arena1'ProxyAdminDatum
            {   arena1'ProxyAdminPKH     ::  PubKeyHash
            ,   arena1'ProxyAdminSession ::  POSIXTime
            }
        deriving
            (Haskell.Eq, Haskell.Ord, Haskell.Show, Generic)

instance Eq Arena1'ProxyDatum where
    {-# INLINABLE (==) #-}
    Arena1'ProxyAdminDatum a b == Arena1'ProxyAdminDatum a' b'
        |   a == a' && b == b' = True
    _   ==  _   = False

PlutusTx.makeIsDataIndexed ''Arena1'ProxyDatum  [ ( 'Arena1'ProxyAdminDatum,    1 )]

data Arena1'ProxyAction
    =   ValidateArena1'ContractsAtMintingA1'GAT
    |   ValidateArena1'ContractsAtVoting
    |   ValidateArena1'ContractsAtBattlefield
    |   Arena1'GladiatorsRevocation
    |   Arena1'ProxyDebugger
    deriving (Haskell.Show)

PlutusTx.makeIsDataIndexed ''Arena1'ProxyAction    [ ( 'ValidateArena1'ContractsAtMintingA1'GAT,   1 )
                                                        , ( 'ValidateArena1'ContractsAtVoting,          2 )
                                                        , ( 'ValidateArena1'ContractsAtBattlefield,     3 )
                                                        , ( 'Arena1'GladiatorsRevocation,                 4 )
                                                        , ( 'Arena1'ProxyDebugger,                 5 )
                                                        ]
