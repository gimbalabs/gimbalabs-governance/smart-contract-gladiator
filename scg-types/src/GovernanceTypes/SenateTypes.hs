
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DerivingVia           #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE ImportQualifiedPost   #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NamedFieldPuns        #-}
{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE UndecidableInstances  #-}

module  GovernanceTypes.SenateTypes
            (   SenateParams  (..)
            ,   SenateAction  (..)
            )
                where

import           GHC.Generics            (Generic)
import           Plutus.V1.Ledger.Crypto (PubKeyHash)
import qualified PlutusTx
import qualified Prelude                 as Haskell

data SenateParams = SenateParams
    {   senatorsPKHs      ::  [PubKeyHash]
    ,   caesarPKH         ::  PubKeyHash
    ,   senateDebuggerPKH ::  PubKeyHash
    }
    deriving
        (Haskell.Eq, Haskell.Ord, Haskell.Show, Generic)

PlutusTx.makeLift ''SenateParams


data SenateAction
    =   Designation
    |   Revocation
    |   Veto
    |   SenateDebugger
    deriving (Haskell.Show)


PlutusTx.makeIsDataIndexed ''SenateAction   [ ( 'Designation,       1 )
                                            , ( 'Revocation,        2 )
                                            , ( 'Veto,              3 )
                                            , ( 'SenateDebugger,    4 )
                                            ]
