
module WriteAll
    (   writeAll
    )
        where

import           Prelude                                   (IO, putStrLn)

import qualified GovernanceCompilers.SenateCompiler        as Senate
import qualified GovernanceToJSON.SenateToJSON             as Senate

import qualified GovernanceCompilers.AccessControlCompiler as AccessControl
import qualified GovernanceToJSON.AccessControlToJSON      as AccessControl

import qualified ArenaOneCompilers.A1DATMinterCompiler     as A1DATMinter
import qualified ArenaOneToJSON.A1DATMinterToJSON          as A1DATMinter

import qualified ArenaOneCompilers.A1GateKeeperCompiler    as A1GateKeeper
import qualified ArenaOneToJSON.A1GateKeeperToJSON         as A1GateKeeper

import qualified ArenaOneCompilers.A1RefUTxOIssuerCompiler as A1RefUTxOIssuer
import qualified ArenaOneToJSON.A1RefUTxOIssuerToJSON      as A1RefUTxOIssuer


writeAll  :: IO ()
writeAll  = do
    putStrLn        "\n<------------------------------------------COMPILING START------------------------------------------>\n"

    Senate.writeScript
    Senate.main

    AccessControl.writeScript
    AccessControl.main

    A1DATMinter.writeScript
    A1DATMinter.main

    A1GateKeeper.writeScript
    A1GateKeeper.main

    A1RefUTxOIssuer.writeScript
    A1RefUTxOIssuer.main

    putStrLn        "\n<-------------------------------------------COMPILING END------------------------------------------->\n"


{-================================================= END OF GENERATOR SECTION =====================================================-}
