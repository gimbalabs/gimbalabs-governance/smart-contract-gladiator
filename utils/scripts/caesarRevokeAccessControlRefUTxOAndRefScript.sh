#!/bin/bash
# mixaxim@mixaxim:~$ chmod +x caesarRevokeAccessControlRefUTxOAndRefScript.sh;./caesarRevokeAccessControlRefUTxOAndRefScript.sh

# Environment Setting
. ./env.conf
reset
clear
set -e

# Logo
printf "\n"
printf "$BLUE%b" "                                                                                             .@.      "
printf "$BLUE%b" "                                                                                            .@@.      "
printf "$BLUE%b" " .#############################.            .########.                      .@@@@@@@@@@@@@@@@@.       "
printf "$BLUE%b" "  .############################.          .########.                       .@@@.           .@@@.      "
printf "$BLUE%b" "   .#######.           .#######.        .########.                        .@@@.              .@@.     "
printf "$BLUE%b" "    .#######.          .#######.      .########.                         .@@@.                .@@.    "
printf "$BLUE%b" "     .#######.         .#######.    .########.                          .@@@.                  .@@.   "
printf "$BLUE%b" "      .#######.                   .########.        .    ..    .       .@@@.                   .@@@.  "
printf "$BLUE%b" "       .#######.                .########.           .        .         .@@@.                  .@@.   "
printf "$BLUE%b" "        .#######.             .########.             ..  ::  ..          .@@@.                 .@.    "
printf "$BLUE%b" "         .#######.          .########.          .. .. .::..::  .. ..      .@@@@.                      "
printf "$BLUE%b" "          .#######.       .########.               ....::..::....          .@@@@@.          .@@.      "
printf "$CYAN%b" "          .########.    .########.           .  :  .  :  ..  :  .  :  .     .@@@@@@@@@@@@@@@@@.       "
printf "$CYAN%b" "          .#######.       .########.               ....::..::....            .@@@@@@@@@@@@@@@@@.      "
printf "$CYAN%b" "         .#######.          .########.          .. ..  ::..::. .. ..                      .@@@@@.     "
printf "$CYAN%b" "        .#######.             .########.             ..  ::  ..                             .@@@@.    "
printf "$CYAN%b" "       .#######.                .########.           .        .                               .@@@.   "
printf "$CYAN%b" "      .#######.                   .########.        .    ..    .                               .@@@.  "
printf "$CYAN%b" "     .#######.         .#######.    .########.                          .@.                     .@@@. "
printf "$CYAN%b" "    .#######.          .#######.      .########.                         .@.                   .@@@.  "
printf "$CYAN%b" "   .#######.           .#######.        .########.                        .@@.                .@@@.   "
printf "$CYAN%b" "  .############################.          .########.                       .@@.              .@@@.    "
printf "$CYAN%b" " .#############################.            .########.                      .@@@.           .@@@.     "
printf "$CYAN%b" "                                                                             .@@@@@@@@@@@@@@@@@.      "
printf "\n"
printf "$YELLOW%b" "                     E K I V A L                                             G I M B A L A B L S    "
printf "$GREEN%b" "                  https://ekival.com                                        https://gimbalabs.com    "
printf "\n"
printf "$RED%b" "<---------- CAESAR REVOKE ACCESS CONTROL REFERENCE UTxO, REFERENCE SCRIPT AND GOVERNOR UTxO ---------->"
printf "\n\n"

# Check Cardano Node is Running
check_process cardano-node

sleep 1

## Collateral Wallet
if [[ -f "/tmp/collat_utxo.json" ]]; then
    rm /tmp/collat_utxo.json
fi
printf "$CYAN%b" "[$(date +%Y-%m-%d\ %H:%M:%S)] Getting Collatral Wallet UTxOs ..."
cardano-cli query utxo \
    --address "$COLLATERAL_ADDRESS" \
    --testnet-magic "$MAGIC_TESTNET_NUMBER" \
    --out-file /tmp/collat_utxo.json
if [[ $(grep -q >/dev/null 2>&1) == $(grep '[^[:space:]]' /tmp/collat_utxo.json) && -f "/tmp/collat_utxo.json" ]]; then
    printf "\n$RED%b\n\n" "[-] ERROR: No Any UTxO Found At Collateral Address"
    exit 1
fi
addressOutputLength=$(jq length /tmp/collat_utxo.json)
if [ "$addressOutputLength" -eq "0" ]; then
    printf "\n$RED%b\n\n" "[-] ERROR: No Any UTxO Found At Collateral Address"
    exit 1
fi
collateralTxIn=$(get_address_biggest_lovelace "$COLLATERAL_ADDRESS")

sleep 1

# Get Wallets UTxOs
if [[ -f "/tmp/caesar.json" ]]; then
    rm /tmp/caesar.json
fi
printf "$CYAN%b" "[$(date +%Y-%m-%d\ %H:%M:%S)] Getting Caesar Address UTxOs ..."
cardano-cli query utxo \
    --address "$CAESAR_ADDRESS" \
    --testnet-magic "$MAGIC_TESTNET_NUMBER" \
    --out-file /tmp/caesar.json
if [[ $(grep -q >/dev/null 2>&1) == $(grep '[^[:space:]]' /tmp/caesar.json) && -f "/tmp/caesar.json" ]]; then
    printf "\n$RED%b\n\n" "[-] ERROR: NO Any UTxOs Found At Caesar Address"
    exit
fi
sleep 1
echo ""
cardano-cli query utxo \
    --address "$CAESAR_ADDRESS" \
    --testnet-magic "$MAGIC_TESTNET_NUMBER"
echo ""
TXNS=$(jq length /tmp/caesar.json)
if [ "$TXNS" -eq "0" ]; then
    printf "\n$RED%b\n\n" "[-] ERROR: NO Any UTxOs Found At Caesar Address"
    exit
fi
caesarTxIN=$(get_address_biggest_lovelace "$CAESAR_ADDRESS")

sleep 1

# Get Senate Reference Script UTxO
printf "$CYAN%b" "[$(date +%Y-%m-%d\ %H:%M:%S)] Getting Senate Reference Script UTxO ..."
if [[ $(grep -q >/dev/null 2>&1) == $(grep '[^[:space:]]' "$TX_PATH"/senate_ref_script.signed) && -f "$TX_PATH/senate_ref_script.signed" ]]; then
    printf "\n$RED%b\n\n" "[-] ERROR: There Is No ACAT Reference Script UTxO"
    exit 1
fi
senateRefScriptUTxO=$(cardano-cli transaction txid --tx-file "$TX_PATH"/senate_ref_script.signed)

sleep 1

# Get Access Control Reference Script UTxO
printf "$CYAN%b" "[$(date +%Y-%m-%d\ %H:%M:%S)] Getting Access Control Reference Script UTxO ..."
if [[ $(grep -q >/dev/null 2>&1) == $(grep '[^[:space:]]' "$TX_PATH"/access_control_ref_script.signed) && -f "$TX_PATH/access_control_ref_script.signed" ]]; then
    printf "\n$RED%b\n\n" "[-] ERROR: There Is No Access Control Reference Script UTxO"
    exit 1
fi
accessControlRefUTxO=$(get_UTxO_by_token "$ACCESS_CONTROL_CONTRACT_ADDRESS" "$ACCESS_CONTROL_REF_UTxO_AUTH_TOKEN")
accessControlRefUTxOLovelace=$(get_UTxO_lovelace_amount "$ACCESS_CONTROL_CONTRACT_ADDRESS" "$accessControlRefUTxO")
accessControGovernorUTxO=$(get_UTxO_by_token "$ACCESS_CONTROL_CONTRACT_ADDRESS" "$ACCESS_CONTROL_GOVERNOR_AUTH_TOKEN")
accessControGovernorUTxOLovelace=$(get_UTxO_lovelace_amount "$ACCESS_CONTROL_CONTRACT_ADDRESS" "$accessControGovernorUTxO")

# sleep 1

# Get Access Control Governor
printf "$CYAN%b" "[$(date +%Y-%m-%d\ %H:%M:%S)] Getting Access Control Contract Address UTxOs ..."
if [[ -f "/tmp/contract_utxo.json" ]]; then
    rm /tmp/contract_utxo.json
fi
cardano-cli query utxo \
    --address "$ACCESS_CONTROL_CONTRACT_ADDRESS" \
    --testnet-magic "$MAGIC_TESTNET_NUMBER" \
    --out-file /tmp/contract_utxo.json
sleep 1
echo ""
cardano-cli query utxo \
    --address "$ACCESS_CONTROL_CONTRACT_ADDRESS" \
    --testnet-magic "$MAGIC_TESTNET_NUMBER"
echo ""
if [[ $(grep -q >/dev/null 2>&1) == $(grep '[^[:space:]]' /tmp/contract_utxo.json) && -f "/tmp/contract_utxo.json" ]]; then
    printf "\n$RED%b\n\n" "[-] ERROR: No Any UTxO Found At Access Control Contract Address"
    exit 1
fi
addressOutputLength=$(jq length /tmp/contract_utxo.json)
if [ "$addressOutputLength" -eq "0" ]; then
    printf "\n$RED%b\n\n" "[-] ERROR: No Any UTxO Found At Access Control Contract Address"
    exit 1
fi

# Tx Output
emperorTreasuryTxOut="$EMPEROR_TREASURY_ADDRESS + $((accessControlRefUTxOLovelace + accessControGovernorUTxOLovelace))"
echo -e "\033[1;32m[+] Tx Output to Emperor Treasury Address: \033[0m" "$emperorTreasuryTxOut"

sleep 1

# Build Tx
printf "$CYAN%b" "[$(date +%Y-%m-%d\ %H:%M:%S)] Building Tx ..."
commandOutput=$(
    cardano-cli transaction build \
        --babbage-era \
        --protocol-params-file "$WALLET_PATH"/protocol.json \
        --testnet-magic "$MAGIC_TESTNET_NUMBER" \
        --out-file "$TX_PATH"/caesar_revoke_access_control.body \
        --change-address "$CAESAR_ADDRESS" \
        --tx-in-collateral="$collateralTxIn" \
        --tx-in="${caesarTxIN}" \
        --tx-in="${accessControGovernorUTxO}" \
        --spending-tx-in-reference="$accessControlRefUTxO" \
        --spending-plutus-script-v2 \
        --spending-reference-tx-in-inline-datum-present \
        --spending-reference-tx-in-redeemer-file "$REVOKE_GOVERNOR_REDEEMER" \
        --tx-in="${accessControlRefUTxO}" \
        --spending-tx-in-reference="$accessControlRefUTxO" \
        --spending-plutus-script-v2 \
        --spending-reference-tx-in-inline-datum-present \
        --spending-reference-tx-in-redeemer-file "$REVOKE_ACCESS_CONTROL_REFERENCE_UTxO_REDEEMER" \
        --tx-out="$emperorTreasuryTxOut" \
        --mint="-1 $ACCESS_CONTROL_REF_UTxO_AUTH_TOKEN + -1 $ACCESS_CONTROL_GOVERNOR_AUTH_TOKEN" \
        --mint-tx-in-reference "$senateRefScriptUTxO#0" \
        --mint-plutus-script-v2 \
        --mint-reference-tx-in-redeemer-file "$REVOCATION_REDEEMER" \
        --policy-id="${SENATE_POLICY_ID}" \
        --required-signer-hash "$CAESAR_PAYMENT_PKH"
)
IFS=':' read -ra outputs <<<"$commandOutput"
IFS=' ' read -ra fee <<<"${outputs[1]}"
echo -e "\033[1;32m[+] Tx Fee:\033[0m" "${fee[1]}"

sleep 1

# Signing Tx
printf "$CYAN%b" "[$(date +%Y-%m-%d\ %H:%M:%S)] Signing Tx ..."
cardano-cli transaction sign \
    --testnet-magic "$MAGIC_TESTNET_NUMBER" \
    --tx-body-file "$TX_PATH"/caesar_revoke_access_control.body \
    --out-file "$TX_PATH"/caesar_revoke_access_control.signed \
    --signing-key-file "$COLLATERAL_PAYMENT_SKEY" \
    --signing-key-file "$CAESAR_PAYMENT_SKEY"

sleep 1

# Submit Tx
printf "$CYAN%b" "[$(date +%Y-%m-%d\ %H:%M:%S)] Submitting Tx ..."
commandOutput=$(cardano-cli transaction submit \
    --testnet-magic "$MAGIC_TESTNET_NUMBER" \
    --tx-file "$TX_PATH"/caesar_revoke_access_control.signed)
sleep 1
echo -e "\033[1;36m[$(date +%Y-%m-%d\ %H:%M:%S)]" "${commandOutput}"

sleep 1

# Tx ID
TXID=$(cardano-cli transaction txid --tx-file "$TX_PATH"/caesar_revoke_access_control.signed)
echo -e "\033[1;32m[+] Transaction ID:\033[0m" "$TXID"

sleep 1

printf "\n$RED%b\n\n" "<----------------------------------------------- DONE ------------------------------------------------>"

#
# Exit
#
