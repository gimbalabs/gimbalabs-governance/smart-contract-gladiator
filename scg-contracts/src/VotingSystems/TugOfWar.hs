
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DerivingVia           #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE UndecidableInstances  #-}

module  VotingSystems.TugOfWar
            (   tugOfWarMintingPolicy
            )
                where

import           Plutus.V1.Ledger.Value    (flattenValue)
import           Plutus.V2.Ledger.Contexts (ScriptContext (..), TxInfo (..),
                                            ownCurrencySymbol)
import           PlutusTx.Prelude          (Bool (..), Integer, all, any, elem,
                                            filter, length, traceIfFalse, ($),
                                            (&&), (==))

{-==============================================================================================================================-}
{-==========                                          MINTING POLICY SECTION                                          ==========-}
{-==============================================================================================================================-}

{-# INLINABLE tugOfWarMintingPolicy #-}
tugOfWarMintingPolicy :: () -> ScriptContext -> Bool
tugOfWarMintingPolicy _ _ = True

{-============================================== END OF MINTING POLICY SECTION =================================================-}
