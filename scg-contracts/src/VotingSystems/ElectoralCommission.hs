
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DerivingVia           #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE UndecidableInstances  #-}

module VotingSystems.ElectoralCommission
    (   electoralCommissionValidator
    )   where

import           Plutus.V2.Ledger.Contexts (ScriptContext (..))
import           PlutusTx.Prelude          (Bool (..))

{-==============================================================================================================================-}
{-==========                                            VALIDATOR SECTION                                             ==========-}
{-==============================================================================================================================-}

{-# INLINABLE electoralCommissionValidator #-}
electoralCommissionValidator ::  () -> () -> ScriptContext -> Bool
electoralCommissionValidator _ _ _ = True

{-================================================== END OF VALIDATOR SECTION ====================================================-}
