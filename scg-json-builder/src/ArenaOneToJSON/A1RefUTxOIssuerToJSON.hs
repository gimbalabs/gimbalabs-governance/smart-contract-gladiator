{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DerivingVia           #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE UndecidableInstances  #-}

module  ArenaOneToJSON.A1RefUTxOIssuerToJSON
            (   main
            ,   arena1'RefUTxOIssuerScriptActivatorUTxODatum
            ,   arena1'GateKeeperRefUTxODatum
            ,   arena1'ProxyRefUTxODatum
            ,   a1'GATMinterRefUTxODatum
            ,   issueArena1'GateKeeperRefUTxORedeemer
            ,   issueArena1'ProxyRefUTxORedeemer
            ,   issueA1'GATMinterRedeemer
            ,   sendBackScriptActivatorRedeemer
            ,   arena1'RefUTxOIssuerDebuggerRedeemer
            )
                where

import           Cardano.Api                        (ScriptDataJsonSchema (ScriptDataJsonDetailedSchema),
                                                     scriptDataToJson)
import           Cardano.Api.Shelley                (fromPlutusData)
import           Data.Aeson                         (encode)
import           Data.ByteString.Lazy               (writeFile)
import           Plutus.V1.Ledger.Address           (Address (..))
import           Plutus.V1.Ledger.Credential        (Credential (PubKeyCredential),
                                                     StakingCredential (StakingHash))
import           Plutus.V1.Ledger.Scripts           (ScriptHash (..),
                                                     ValidatorHash (..))
import           Plutus.V1.Ledger.Value             (CurrencySymbol)
import           PlutusTx                           (ToData, toData)
import           PlutusTx.Prelude                   (Bool (..), Maybe (..), ($),
                                                     (.), (<>))
import qualified Prelude                            as Haskell
import           System.Directory                   (createDirectoryIfMissing)
import           System.FilePath.Posix              ((<.>), (</>))

import           ArenaOneTypes.A1RefUTxOIssuerTypes (Arena1'RefUTxOIssuerAction (..),
                                                     Arena1'RefUTxOIssuerDatum (..))
import           Validators                         (a1'GATCurrencySymbol,
                                                     arena1'ProxyValidatorHash)

{-------------------REPUBLIC TREASURY INFORMATION---------------------}

republicTreasuryPaymentPKH :: Credential
republicTreasuryPaymentPKH = PubKeyCredential "a512a21ba616d7b8897445ad269d78f27185a3e8aa553774d36fa967"

republicTreasuryStakePKH :: Maybe StakingCredential
republicTreasuryStakePKH = Just $ StakingHash $ PubKeyCredential "1f7455d9dd124a036d4cfa09b8d7c5c231927f6d52a4c59d3239c450"

republicTreasuryAddress' :: Address
republicTreasuryAddress' = Address republicTreasuryPaymentPKH republicTreasuryStakePKH

{-------------------------------------------------------------------}

{--------------------ARENA 1 DOMAIN INFORMATION---------------------}

validatorHashToScriptHash :: ValidatorHash -> ScriptHash
validatorHashToScriptHash (ValidatorHash hash) = ScriptHash hash

arena1'ProxyVH' :: ValidatorHash
arena1'ProxyVH' =  arena1'ProxyValidatorHash

arena1'ProxySH' :: ScriptHash
arena1'ProxySH' = validatorHashToScriptHash arena1'ProxyVH'

currencySymbolOfA1'GAT' :: CurrencySymbol
currencySymbolOfA1'GAT' = a1'GATCurrencySymbol

{-------------------------------------------------------------------}

{------------------------------DATUMS-------------------------------}

arena1'RefUTxOIssuerScriptActivatorUTxODatum :: Arena1'RefUTxOIssuerDatum
arena1'RefUTxOIssuerScriptActivatorUTxODatum =
    Arena1'RefUTxOIssuerScriptActivatorDatum 42

arena1'GateKeeperRefUTxODatum :: Arena1'RefUTxOIssuerDatum
arena1'GateKeeperRefUTxODatum =
    Arena1'GateKeeperRefUTxODatum
        republicTreasuryAddress'
        arena1'ProxySH'
        currencySymbolOfA1'GAT'

arena1'ProxyRefUTxODatum :: Arena1'RefUTxOIssuerDatum
arena1'ProxyRefUTxODatum =
    Arena1'ProxyRefUTxODatum
        currencySymbolOfA1'GAT'

a1'GATMinterRefUTxODatum :: Arena1'RefUTxOIssuerDatum
a1'GATMinterRefUTxODatum =
    A1'GATMinterRefUTxODatum
        arena1'ProxyVH'

{-------------------------------------------------------------------}

{-----------------------------REDEEMERS-----------------------------}

issueArena1'GateKeeperRefUTxORedeemer :: Arena1'RefUTxOIssuerAction
issueArena1'GateKeeperRefUTxORedeemer = IssueArena1'GateKeeperRefUTxO

issueArena1'ProxyRefUTxORedeemer :: Arena1'RefUTxOIssuerAction
issueArena1'ProxyRefUTxORedeemer = IssueArena1'ProxyRefUTxO

issueA1'GATMinterRedeemer :: Arena1'RefUTxOIssuerAction
issueA1'GATMinterRedeemer = IssueA1'GATMinterRefUTxO

sendBackScriptActivatorRedeemer :: Arena1'RefUTxOIssuerAction
sendBackScriptActivatorRedeemer = SendBackScriptActivatorUTxO

arena1'RefUTxOIssuerDebuggerRedeemer :: Arena1'RefUTxOIssuerAction
arena1'RefUTxOIssuerDebuggerRedeemer = Arena1'RefUTxOIssuerDebugger

{-------------------------------------------------------------------}


writeJSON :: ToData a => Haskell.FilePath -> a -> Haskell.IO ()
writeJSON file = writeFile file . encode . scriptDataToJson ScriptDataJsonDetailedSchema . fromPlutusData . toData

main :: Haskell.IO ()
main = do
        let datDir          = "../dist/data/datums"
            redDir          = "../dist/data/redeemers"
            contractName    = "A1RefUTxOIssuer"
        createDirectoryIfMissing True $ datDir </> contractName
        createDirectoryIfMissing True $ redDir </> contractName

        writeJSON (datDir </> contractName </> "arena_1_ref_utxo_issuer_script_activator_utxo_datum"    <.> "json") arena1'RefUTxOIssuerScriptActivatorUTxODatum
        writeJSON (datDir </> contractName </> "arena_1_gate_keeper_ref_utxo_datum"                     <.> "json") arena1'GateKeeperRefUTxODatum
        writeJSON (datDir </> contractName </> "arena_1_proxy_ref_utxo_datum"                           <.> "json") arena1'ProxyRefUTxODatum
        writeJSON (datDir </> contractName </> "A1GAT_minter_ref_utxo_datum"                            <.> "json") a1'GATMinterRefUTxODatum

        writeJSON (redDir </> contractName </> "issue_arena_1_gate_keeper_ref_utxo_redeemer"    <.> "json") issueArena1'GateKeeperRefUTxORedeemer
        writeJSON (redDir </> contractName </> "issue_arena_1_proxy_ref_utxo_redeemer"          <.> "json") issueArena1'ProxyRefUTxORedeemer
        writeJSON (redDir </> contractName </> "issue_A1GAT_minter_ref_utxo_redeemer"           <.> "json") issueA1'GATMinterRedeemer
        writeJSON (redDir </> contractName </> "send_back_script_activator_redeemer"            <.> "json") sendBackScriptActivatorRedeemer
        writeJSON (redDir </> contractName </> "arena_1_ref_utxo_issuer_debugger_redeemer"      <.> "json") arena1'RefUTxOIssuerDebuggerRedeemer


        Haskell.putStrLn        "\n<--------------------------------------DONE------------------------------------------>"
        Haskell.putStrLn        "   ATTENTION:"
        Haskell.putStrLn    $   "       Datums location:    " <> Haskell.show ( datDir </> contractName)
        Haskell.putStrLn    $   "       Redeemers location: " <> Haskell.show (redDir </> contractName)
        Haskell.putStrLn        "<------------------------------------------------------------------------------------>\n"
