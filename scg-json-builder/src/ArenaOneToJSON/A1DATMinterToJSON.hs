{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DerivingVia           #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE UndecidableInstances  #-}

module ArenaOneToJSON.A1DATMinterToJSON
        (   main
        ,   governorMintA1'DATRedeemer
        ,   governorBurnA1'DATRedeemer
        ,   a1'DATMinterDebuggerRedeemer
        )
            where

import           Cardano.Api                    (ScriptDataJsonSchema (ScriptDataJsonDetailedSchema),
                                                 scriptDataToJson)
import           Cardano.Api.Shelley            (fromPlutusData)
import           Data.Aeson                     (encode)
import           Data.ByteString.Lazy           (writeFile)
import           PlutusTx                       (ToData, toData)
import           PlutusTx.Prelude               (Bool (..), ($), (.), (<>))
import qualified Prelude                        as Haskell
import           System.Directory               (createDirectoryIfMissing)
import           System.FilePath.Posix          ((<.>), (</>))

import           ArenaOneTypes.A1DATMinterTypes (Arena1'DATAction (..))

{-----------------------------REDEEMERS-----------------------------}

governorMintA1'DATRedeemer :: Arena1'DATAction
governorMintA1'DATRedeemer = GovernorMintA1'DAT

governorBurnA1'DATRedeemer :: Arena1'DATAction
governorBurnA1'DATRedeemer = GovernorBurnA1'DAT

a1'DATMinterDebuggerRedeemer :: Arena1'DATAction
a1'DATMinterDebuggerRedeemer = A1'DATMinterDebugger

{--------------------------------------------------------------------}


writeJSON :: ToData a => Haskell.FilePath -> a -> Haskell.IO ()
writeJSON file = writeFile file . encode . scriptDataToJson ScriptDataJsonDetailedSchema . fromPlutusData . toData

main :: Haskell.IO ()
main = do
        let redDir          = "../dist/data/redeemers"
            contractName    = "A1DATMinter"
        createDirectoryIfMissing True $ redDir </> contractName

        writeJSON (redDir </> contractName </> "governor_mint_A1DAT_redeemer"   <.> "json") governorMintA1'DATRedeemer
        writeJSON (redDir </> contractName </> "governor_burn_A1DAT_redeemer"   <.> "json") governorBurnA1'DATRedeemer
        writeJSON (redDir </> contractName </> "A1DAT_minter_debugger_redeemer" <.> "json") a1'DATMinterDebuggerRedeemer

        Haskell.putStrLn        "\n<--------------------------------------DONE------------------------------------------>"
        Haskell.putStrLn        "   ATTENTION:"
        Haskell.putStrLn    $   "       Redeemers location: " <> Haskell.show (redDir </> contractName)
        Haskell.putStrLn        "<------------------------------------------------------------------------------------>\n"
