{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DerivingVia           #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE UndecidableInstances  #-}

module VotingSystemsToJSON.TugOfWarToJSON
                (   main
                )
                    where

import           Cardano.Api           (ScriptDataJsonSchema (ScriptDataJsonDetailedSchema),
                                        scriptDataToJson)
import           Cardano.Api.Shelley   (fromPlutusData)
import           Data.Aeson            (encode)
import           Data.ByteString.Lazy  (writeFile)
import           PlutusTx              (ToData, toData)
import           PlutusTx.Prelude      (Bool (..), ($), (.), (<>))
import qualified Prelude               as Haskell
import           System.Directory      (createDirectoryIfMissing)
import           System.FilePath.Posix ((<.>), (</>))


writeJSON :: ToData a => Haskell.FilePath -> a -> Haskell.IO ()
writeJSON file = writeFile file . encode . scriptDataToJson ScriptDataJsonDetailedSchema . fromPlutusData . toData

main :: Haskell.IO ()
main = do
        let redDir          = "../dist/data/redeemers"
            contractName    = "TugOfWarMinter"
        createDirectoryIfMissing True $ redDir </> contractName

        writeJSON (redDir </> contractName </> ""  <.> "json")

        Haskell.putStrLn        "\n<--------------------------------------DONE------------------------------------------>"
        Haskell.putStrLn        "   ATTENTION:"
        Haskell.putStrLn    $   "       Redeemers location: " <> Haskell.show (redDir </> contractName)
        Haskell.putStrLn        "<------------------------------------------------------------------------------------>\n"
