{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DerivingVia           #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE UndecidableInstances  #-}

module  VotingSystemsToJSON.ElectoralCommissionToJSON
                (   main
                )
                    where

import           Cardano.Api           (ScriptDataJsonSchema (ScriptDataJsonDetailedSchema),
                                        scriptDataToJson)
import           Cardano.Api.Shelley   (fromPlutusData)
import           Data.Aeson            (encode)
import           Data.ByteString.Lazy  (writeFile)
import           PlutusTx              (ToData, toData)
import           PlutusTx.Prelude      (Bool (False, True), ($), (.), (<>))
import           System.Directory      (createDirectoryIfMissing)
import           System.FilePath.Posix ((<.>), (</>))

import qualified Prelude               as Haskell



writeJSON :: ToData a => Haskell.FilePath -> a -> Haskell.IO ()
writeJSON file = writeFile file . encode . scriptDataToJson ScriptDataJsonDetailedSchema . fromPlutusData . toData

main :: Haskell.IO ()
main = do
        let datDir          = "dist/data/datums"
            redDir          = "dist/data/redeemers"
            contractName    = "ElectoralCommission"
        createDirectoryIfMissing True $ datDir </> contractName
        createDirectoryIfMissing True $ redDir </> contractName

        writeJSON (datDir </> contractName </> ""     <.> "json")

        writeJSON (redDir </> contractName </> ""      <.> "json")

        Haskell.putStrLn        "\n<--------------------------------------DONE------------------------------------------>"
        Haskell.putStrLn        "   ATTENTION:"
        Haskell.putStrLn    $   "       Datums location:    " <> Haskell.show ( datDir </> contractName)
        Haskell.putStrLn    $   "       Redeemers location: " <> Haskell.show (redDir </> contractName)
        Haskell.putStrLn        "<------------------------------------------------------------------------------------>\n"
