{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DerivingVia           #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE ImportQualifiedPost   #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NamedFieldPuns        #-}
{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE UndecidableInstances  #-}

module  Validators
            (   arena1'RefUTxOIssuerValidatorHash
            ,   a1'DATCurrencySymbol
            ,   mkArena1'DomainAuthTokenMintingPolicy
            ,   mkArena1'GladiatorAuthTokenMintingPolicy
            ,   arena1'GateKeeperValidatorHash
            ,   a1'GATCurrencySymbol
            ,   arena1'ProxyValidatorHash
            )
                    where

import qualified Plutonomy
import           Plutus.Script.Utils.Typed          (mkUntypedMintingPolicy,
                                                     mkUntypedValidator)
import           Plutus.Script.Utils.V2.Scripts     (scriptCurrencySymbol,
                                                     validatorHash)
import           Plutus.V1.Ledger.Crypto            (PubKeyHash)
import           Plutus.V1.Ledger.Scripts           (MintingPolicy, Validator,
                                                     ValidatorHash,
                                                     mkMintingPolicyScript,
                                                     mkValidatorScript)
import           Plutus.V1.Ledger.Value             (CurrencySymbol)
import           PlutusTx
import           PlutusTx.Prelude                   (($), (.))
-- import           Prelude                        (IO, putStrLn, show, (<>))

import           ArenaOneContracts.A1DATMinter      (arena1'DomainAuthTokenMintingPolicy)
import           ArenaOneContracts.A1GATMinter      (arena1'GladiatorAuthTokenMintingPolicy)
import           ArenaOneContracts.A1GateKeeper     (arena1'GateKeeperValidator)
import           ArenaOneContracts.A1Proxy          (arena1'ProxyValidator)
import           ArenaOneContracts.A1RefUTxOIssuer  (arena1'RefUTxOIssuerValidator)
import           ArenaOneTypes.A1DATMinterTypes     (Arena1'DATParams (..))
import           ArenaOneTypes.A1GATMinterTypes     (Arena1'GATParams (..))
import           ArenaOneTypes.A1GateKeeperTypes    (Arena1'GateKeeperParams (..))
import           ArenaOneTypes.A1ProxyTypes         (Arena1'ProxyParams (..))
import           ArenaOneTypes.A1RefUTxOIssuerTypes (Arena1'RefUTxOIssuerParams (..))
import           GovernanceContracts.AccessControl  (accessControlValidator)
import           GovernanceContracts.Senate         (senateMintingPolicy)
import           GovernanceTypes.AccessControlTypes (AccessControlParams (..))
import           GovernanceTypes.SenateTypes        (SenateParams (..))


debuggerPKH :: PubKeyHash
debuggerPKH = "a6a888d4e2f5245a8883b81a339173373bbf5d163b4be2cceff5673c"

{-------------------------------SENATE------------------------------}

senateParams :: SenateParams
senateParams =
    SenateParams
        {   senatorsPKHs =
                [   "61aef8fc637e65b9fc862e45229b97d15e686bd463491f2cb003d113"
                ,   "aca061ea392296a8390152b59fb6dc8351df9da421083ea4ff3235a3"
                ,   "742ba6843a858d12dc8600ceab5d2b9ced8bd220b8cfc37d09c08273"
                ,   "19c6d778560e0a0fff7edcae3292c910e3403e031fac10043d7d7e67"
                ,   "49749e57887296bfaddc403966f12e0a2bbd6103bc73f2bcc0139634"
                ,   "64a8b1f900d30038e2c379e988457ae2ab09e014117d9fe064ef3d60"
                ,   "640e32f08e478d1eb0a56d02966f3d8f6fd22a54b5a371e4bb3a0f15"
                ]
        ,   caesarPKH = "adb174eb050b41f8d8ab9532e2feed52626b1ef5703168caf3373ebb"
        ,   senateDebuggerPKH = debuggerPKH
        }

mkSenateMintingPolicy :: MintingPolicy
mkSenateMintingPolicy =
    Plutonomy.optimizeUPLC
        $   mkMintingPolicyScript
            $   $$(compile [|| mkUntypedMintingPolicy . senateMintingPolicy ||])
            `applyCode` liftCode  senateParams

senatePolicyID' :: CurrencySymbol
senatePolicyID' =
    scriptCurrencySymbol mkSenateMintingPolicy

{------------------------------------------------------------------}

{------------------------ACCESS CONTROL----------------------------}

accessControlParams :: AccessControlParams
accessControlParams =
    AccessControlParams
        senatePolicyID'
        debuggerPKH

mkAccessControlValidator :: Validator
mkAccessControlValidator =
    Plutonomy.optimizeUPLC
        $   mkValidatorScript
            $   $$(compile [|| mkUntypedValidator . accessControlValidator ||])
                `applyCode` liftCode accessControlParams

accessControlValidatorHash ::  ValidatorHash
accessControlValidatorHash =
    validatorHash mkAccessControlValidator

{------------------------------------------------------------------}

{---------------------ARENA 1 REF UTXO ISSUER ---------------------}

arena1'RefUTxOIssuerParams :: Arena1'RefUTxOIssuerParams
arena1'RefUTxOIssuerParams =
    Arena1'RefUTxOIssuerParams
        accessControlValidatorHash
        senatePolicyID'
        debuggerPKH

mkArena1'RefUTxOIssuerValidator ::  Validator
mkArena1'RefUTxOIssuerValidator =
    Plutonomy.optimizeUPLC
        $   mkValidatorScript
            $   $$(compile [|| mkUntypedValidator . arena1'RefUTxOIssuerValidator ||])
                `applyCode` liftCode arena1'RefUTxOIssuerParams

arena1'RefUTxOIssuerValidatorHash :: ValidatorHash
arena1'RefUTxOIssuerValidatorHash =
    validatorHash mkArena1'RefUTxOIssuerValidator

{------------------------------------------------------------------}

{-------------------------------A1'DAT------------------------------}

arena1'DATParams :: Arena1'DATParams
arena1'DATParams =
    Arena1'DATParams
        accessControlValidatorHash
        debuggerPKH

mkArena1'DomainAuthTokenMintingPolicy :: MintingPolicy
mkArena1'DomainAuthTokenMintingPolicy =
    Plutonomy.optimizeUPLC
        $   mkMintingPolicyScript
            $   $$(compile [|| mkUntypedMintingPolicy . arena1'DomainAuthTokenMintingPolicy ||])
            `applyCode` liftCode  arena1'DATParams

a1'DATCurrencySymbol :: CurrencySymbol
a1'DATCurrencySymbol =
    scriptCurrencySymbol mkArena1'DomainAuthTokenMintingPolicy

{------------------------------------------------------------------}

{---------------------ARENA 1 GATE KEEPER--------------------------}

arena1'GateKeeperParams :: Arena1'GateKeeperParams
arena1'GateKeeperParams =
    Arena1'GateKeeperParams
        accessControlValidatorHash
        a1'DATCurrencySymbol
        debuggerPKH


mkArena1'GateKeeperValidator :: Validator
mkArena1'GateKeeperValidator =
    Plutonomy.optimizeUPLC
        $   mkValidatorScript
            $   $$(compile [|| mkUntypedValidator . arena1'GateKeeperValidator ||])
                `applyCode` liftCode arena1'GateKeeperParams

arena1'GateKeeperValidatorHash :: ValidatorHash
arena1'GateKeeperValidatorHash =
    validatorHash mkArena1'GateKeeperValidator

{------------------------------------------------------------------}

{------------------------ARENA 1 PROXY-----------------------------}

arena1'ProxyParams :: Arena1'ProxyParams
arena1'ProxyParams =
    Arena1'ProxyParams
        arena1'GateKeeperValidatorHash
        debuggerPKH

mkArena1'ProxyValidator :: Validator
mkArena1'ProxyValidator =
    Plutonomy.optimizeUPLC
        $   mkValidatorScript
            $   $$(compile [|| mkUntypedValidator . arena1'ProxyValidator ||])
                `applyCode` liftCode arena1'ProxyParams

arena1'ProxyValidatorHash :: ValidatorHash
arena1'ProxyValidatorHash =
    validatorHash mkArena1'ProxyValidator

{------------------------------------------------------------------}
{-------------------------------A1'GAT------------------------------}

arena1'GATParams :: Arena1'GATParams
arena1'GATParams =
    Arena1'GATParams
        accessControlValidatorHash
        debuggerPKH

mkArena1'GladiatorAuthTokenMintingPolicy :: MintingPolicy
mkArena1'GladiatorAuthTokenMintingPolicy =
    Plutonomy.optimizeUPLC
        $   mkMintingPolicyScript
            $   $$(compile [|| mkUntypedMintingPolicy . arena1'GladiatorAuthTokenMintingPolicy ||])
            `applyCode` liftCode  arena1'GATParams

a1'GATCurrencySymbol :: CurrencySymbol
a1'GATCurrencySymbol =
    scriptCurrencySymbol mkArena1'GladiatorAuthTokenMintingPolicy

{------------------------------------------------------------------}
